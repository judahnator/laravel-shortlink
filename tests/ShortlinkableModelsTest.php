<?php

namespace judahnator\LaravelShortlink\Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use judahnator\LaravelShortlink\Models\Shortlink;
use judahnator\LaravelShortlink\ServiceProvider;
use judahnator\LaravelShortlink\Traits\Shortlinkable;
use Orchestra\Testbench\TestCase;

final class ShortlinkableModelsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate');
        FooModel::migrate();
        BarModel::migrate();
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => ''
        ]);
        parent::getEnvironmentSetUp($app);
    }

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    public function testCreatingShortlinkableModels(): void
    {
        /** @var TestModel $fooModel */
        $fooModel = (new FooModel());
        $fooModel->save();

        /** @var Shortlink $fooShortlink */
        $fooShortlink = $fooModel->shortlink()->create();

        /** @var TestModel $barModel */
        $barModel = (new BarModel());
        $barModel->save();

        /** @var Shortlink $barShortlink */
        $barShortlink = $barModel->shortlink()->create();

        $this->assertLessThan(strlen($fooModel->getLinkHrefAttribute()), strlen($fooShortlink->getLinkHrefAttribute()));
        $this->assertLessThan(strlen($barModel->getLinkHrefAttribute()), strlen($barShortlink->getLinkHrefAttribute()));

        $this->assertNotSame($fooModel->getLinkHrefAttribute(), $barModel->getLinkHrefAttribute());

        $this->get($fooShortlink->getLinkHrefAttribute())->assertRedirect($fooModel->getLinkHrefAttribute());
        $this->get($barShortlink->getLinkHrefAttribute())->assertRedirect($barModel->getLinkHrefAttribute());
    }
}

abstract class TestModel extends Model
{
    use Shortlinkable;

    public $timestamps = false;

    public static function migrate(): void
    {
        $table = (new static())->table;
        Schema::dropIfExists($table);
        Schema::create($table, function (Blueprint $table): void {
            $table->increments('id');
        });
    }

    /**
     * Returns the route to this resource.
     * Example: `return route('resource.show', $this);`
     *
     * @return string
     */
    public function getLinkHrefAttribute(): string
    {
        return "http://localhost/{$this->table}/{$this->attributes['id']}";
    }
}

final class FooModel extends TestModel
{
    protected $table = 'foomodel';
}

final class BarModel extends TestModel
{
    protected $table = 'barmodel';
}
