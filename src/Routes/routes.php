<?php

use Illuminate\Support\Facades\Route;
use judahnator\LaravelShortlink\Controllers\ShortlinkController;

Route::get('/s/{shortlink}', ShortlinkController::class.'@show')->name('shortlink');
