<?php

namespace judahnator\LaravelShortlink\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use judahnator\LaravelShortlink\Models\Shortlink;

trait Shortlinkable
{

    /**
     * Returns the route to this resource.
     * Example: `return route('resource.show', $this);`
     *
     * @return string
     */
    abstract public function getLinkHrefAttribute(): string;

    /**
     * We must be in a model. Nothing else is supported.
     */
    final public function initializeShortlinkable(): void
    {
        if (!$this instanceof Model) {
            throw new \RuntimeException('Shortlinkable items must be models.');
        }
    }

    /**
     * Returns the relationship with the Shortlink model.
     *
     * @return MorphOne
     */
    final public function shortlink(): MorphOne
    {
        return $this->morphOne(Shortlink::class, 'shortlinkable');
    }
}
