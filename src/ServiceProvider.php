<?php

namespace judahnator\LaravelShortlink;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

final class ServiceProvider extends BaseServiceProvider
{
    public function boot(): void
    {
        $this->loadMigrationsFrom(['--path' => realpath(__DIR__.'/Migrations'),]);
        $this->loadRoutesFrom(__DIR__.'/Routes/routes.php');
    }
}
