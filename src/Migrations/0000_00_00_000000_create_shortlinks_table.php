<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class CreateShortlinksTable extends Migration
{
    public function up(): void
    {
        Schema::create('shortlinks', function (Blueprint $table): void {
            $table->uuid('uuid');
            $table->string('url')->unique();
            $table->string('shortlinkable_type');
            $table->string('shortlinkable_id');
        });
    }
}
