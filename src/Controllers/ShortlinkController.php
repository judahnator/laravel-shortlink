<?php

namespace judahnator\LaravelShortlink\Controllers;

use Illuminate\Routing\Controller;
use judahnator\LaravelShortlink\Models\Shortlink;
use judahnator\LaravelShortlink\Traits\Shortlinkable;

final class ShortlinkController extends Controller
{
    public function show(string $shortlink)
    {
        $shortlink = Shortlink::where('url', $shortlink)->firstOrFail();

        /** @var Shortlinkable $shortlinkable */
        $shortlinkable = $shortlink->shortlinkable()->first();
        return response()->redirectTo($shortlinkable->getLinkHrefAttribute());
    }
}
