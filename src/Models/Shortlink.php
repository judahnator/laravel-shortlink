<?php

namespace judahnator\LaravelShortlink\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use judahnator\LaravelShortlink\Traits\Shortlinkable;
use Ramsey\Uuid\Uuid;

final class Shortlink extends Model
{
    use Shortlinkable;

    protected $appends = [
        'link_href'
    ];

    public $incrementing = false;

    protected $primaryKey = 'uuid';

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Shortlink $model): void {

            // Inject the primary key
            $model->{$model->primaryKey} = Uuid::uuid4()->toString();

            // If the URL was not manually set, generate one
            if (!($model->attributes['url'] ?? false)) {
                $uniqid = uniqid(); // generate a uniqid for this item
                $characters = 3; // the minimum number of characters.
                do {
                    $url = substr($uniqid, 0, $characters);
                    $characters++;
                    if ($characters > strlen($uniqid)) {
                        abort(500, 'Entropy is dead');
                    }
                } while (static::where('url', $url)->exists());
                $model->attributes['url'] = $url;
            }
        });
    }

    /**
     * Returns the route to this resource.
     *
     * @return string
     */
    public function getLinkHrefAttribute(): string
    {
        return route('shortlink', $this);
    }

    /**
     * Overrides the route key name to the url column.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url';
    }

    /**
     * The morph relationship.
     *
     * @return MorphTo
     */
    public function shortlinkable(): MorphTo
    {
        return $this->morphTo(__FUNCTION__);
    }
}
