Laravel Link Shortener
======================

[![pipeline status](https://gitlab.com/judahnator/laravel-shortlink/badges/master/pipeline.svg)](https://gitlab.com/judahnator/laravel-shortlink/commits/master)

Real quick, here is the short version:

Who
---
You

What
----
This library allows you to shorten any link to a model in your project

When
----
Right now, get with it

Where
-----
The interwebs

Why
---
Because sometimes URLs are too long

How
---
See below.

___

Long Version
------------

Lets say you have a message board where the message model uses UUIDs for their primary key. Your URL might look like this: '/message/2cfbb523-ee42-484a-ac2a-fc55c8ce03d9'

Not exactly a sharable link. This library can help.

**Installation**

Its a simple composer package, just install it via that. If you are using Laraval 5.6+ the service provider will automagically register for you.

`composer require judahnator/laravel-shortlink`

Next you just need to run the migrations.

`php artisan migrate`

**Setup**

In your models that you wish to have a shorter link for, simply use the `Shortlinkable` trait. It has an abstract `getLinkHrefAttribute()` method that you will also need to create.

```php
<?php

use Illuminate\Database\Eloquent\Model;
use judahnator\LaravelShortlink\Traits\Shortlinkable;

class Message extends Model
{
    
    // Stick this here
    use Shortlinkable;
    
    // You will be required to add this function too.
    // It needs to return the link to the route to display the resource.
    public function getLinkHrefAttribute(): string
    {
        return route('messages.show', $this);
    }
    
    // ...
}
```

**Usage**

"Short Links" are not created by default. If you want to create one manually, you can do that like this:
```php
<?php
/** @var \judahnator\LaravelShortlink\Traits\Shortlinkable $message */
$shortlink = $message->shortlink()->create();

// You now have a shortened link
echo $shortlink->link_href;
```

You can also have one created automatically when a new model instance is generated. Not sure why you would want that, but you can totally do it.

```php
<?php

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    // all the other stuff
    
    protected static function boot(): void
    {
        parent::boot();
        static::created(function(Message $message): void {
            $message->shortlink()->create();
        });
    }

}
```

**Routing**

The URLs generated are as short as possible. The more frequently a new one is made the longer they need to be. If you only need a new shortened link every few days you can get away with three or four characters, but you can make one every few milliseconds and it should stay under 10.

The "short links" are all under the "/s" prefix.